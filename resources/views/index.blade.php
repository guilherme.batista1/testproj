<form>
    <div class="form-row">
        <div class="col-moedas">
            <label class="moedas" for="inlineFormCustomSelect">Moeda Atual</label>
            <select class="custom-select mr-sm-2" id="moedas">
            @foreach ($currents as $current)
                <option selected>$current</option>
            @endforeach
            </select>
        </div>
        <div class="form-value">
            <label for="inputValue">Valor</label>
            <input type="text" class="form-control" id="inputValue" placeholder="Value">
        </div>
        <div class="col-moedas">
            <label class="moedas2" for="inlineFormCustomSelect">Moeda para Conversão</label>
            <select class="custom-select mr-sm-2" id="moedas2">
            @foreach ($currents as $current)
                <option selected>$current</option>
            @endforeach
            </select>
        </div>
        <div class="btnSub">
            <button type="submit" class="btn btn-primary">OK</button>
        </div>
    </div>
</form>
<div class="alert alert-success">
  <strong>Success!</strong> Indicates a successful or positive action.
</div>