<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function getCurrencys(){
        $arq = file_get_contents('../../../../currencys.json');
 
        // Decodifica o formato JSON e retorna um Objeto
        $json = json_decode($arq);
        $idCurrency = array();

        // Loop para percorrer o Objeto
        
        foreach($json as $Currency):
            $value = $Currency->id;  
            idCurrency.sort($value); 
        endforeach;

        return idCurrency; 
        //return view
    }

    function convertCurrency($amount,$from_currency,$to_currency){
        $apikey = '420510e98ae763980439';
      
        $from_Currency = urlencode($from_currency);
        $to_Currency = urlencode($to_currency);
        $query =  "{$from_Currency}_{$to_Currency}";
      
        // change to the free URL if you're using the free version
        $json = file_get_contents("https://api.currconv.com/api/v7/convert?q={$query}&compact=ultra&apiKey={$apikey}");
        $obj = json_decode($json, true);
      
        $val = floatval($obj["$query"]);
      
      
        $total = $val * $amount;
        return number_format($total, 2, '.', '');
        //retornar view
      }
}